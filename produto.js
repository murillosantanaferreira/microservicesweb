new Vue({
    el:'#app',
    data:{
        modalTxtAction:'',
        modalBtnAction: '',
        product:{}
    },
    created:function(){

    },
    methods:{
        changeModalSave: function(){
            this.modalTxtAction =  'Deseja salvar este produto?'
            this.modalBtnAction = 'salvar';
            $('#modal-confirm-action').modal('show');    
            $('#modal-form-product').modal('hide');            
        },
        changeModalDelete: function(){
            this.modalTxtAction = 'Deseja excluir este produto?'
            this.modalBtnAction = 'confirmar';
        },
        save: function(){

        },
        cancelAction: function(){
            $('#modal-form-product').modal('show');
        }
    }
});

// var changeModal = function(textContent, textBtn){
//     $('#modal-txt-action').text(textContent);
//     $('#modal-btn-action').html(textBtn);    
// }

// var changeModalSave = function(){
//     changeModal('Deseja salvar este produto?', 'salvar');
// }

// var changeModalDelete = function(){
//     changeModal('Deseja excluir este produto?', 'confirmar');
// }